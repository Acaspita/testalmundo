angular
  .module('app', [
    'lbServices',
    'ui.router'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider,
      $urlRouterProvider) {
    $stateProvider
      .state('hotel', {
        url: '',
        templateUrl: 'views/hotels.html',
        controller: 'HotelsController'
      });

    $urlRouterProvider.otherwise('hotel');
  }]);