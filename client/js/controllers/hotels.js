angular
  .module('app')
  .controller('HotelsController', ['$scope', '$state', 'Hotel', function($scope, $state, Hotel) {
    
    $scope.getNumber = function(num) {
        return new Array(num);   
    };

    $scope.getHotels = function(stars, hotelNameFilter) {
      Hotel
        .find({where: {and: [{name: {like: "%" + hotelNameFilter + "%"}},
                              {or: [{stars: (stars[0]? 1:0)},
                                    {stars: (stars[1]? 2:0)},
                                    {stars: (stars[2]? 3:0)},
                                    {stars: (stars[3]? 4:0)},
                                    {stars: (stars[4]? 5:0)}
                              ]}
                            ]}
                      })
        .$promise
        .then(function(results) {
          $scope.hotels = results;
        });
    };

    $scope.filtrarNombre = function() {
      var checkValues = [$scope.star1,$scope.star2,$scope.star3,$scope.star4,$scope.star5];
      $scope.getHotels(checkValues, $scope.hotelNameFilter);
    };

    $scope.setearTodos = function() {
      $scope.star1 = $scope.star2 =
      $scope.star3 = $scope.star4 =
      $scope.star5 = $scope.todos;
    };

    $scope.$watchGroup(['star1','star2','star3','star4','star5'], function(newValues, oldValues) {
      if (newValues[0]&newValues[1]&newValues[2]&newValues[3]&newValues[4]) {
        $scope.todos = true;
      }
      else if (newValues[0]|newValues[1]|newValues[2]|newValues[3]|newValues[4]) {
        $scope.todos = false;
      }
      $scope.getHotels(newValues, $scope.hotelNameFilter);
    });

    $scope.hotels = [];
    $scope.hotelNameFilter = "";
    $scope.todos = true;
    $scope.star1 = $scope.star2 =
    $scope.star3 = $scope.star4 =
    $scope.star5 = true;
    var stars = [true, true, true, true, true];
    $scope.getHotels(stars, "");
  }]);