Test Evaluación para AlMundo:
-----------------------------

Esta aplicación se construyó utilizando el framework Loopback el cual está construido sobre Express.


Instalación:
------------

Con node instalado, ejecutar sobre la carpeta de esta aplicación "npm install".

Para correr la aplicación, ejeutar sobre la misma carpeta "npm start".

La aplicación estará disponible en localhost:3000.


CRUD API:
---------

Se podrá explorar la API en la aplicación Swagger disponible en localhost:3000/explorer.

La lógica de la aplicación se encuentra en /client/js/controllers/hotels.js

Nota:
-----

Por cuestiones de tiempo, no he llegado a encontrar en la documentación la razón por la que no funciona el filtro la consulta que se hace en hotels.js:11 . La misma es equivalente a la siguiente, que se hace a través de este link, o incluso mediante Swagger:

(No Codificado)
http://localhost:3000/api/Hotels?filter={"where":{"and":[{"name":{"like":"%Casa%"}},{"stars":4}]}}
(Codificado. Link funciona)
http://localhost:3000/api/Hotels?filter=%7B%22where%22%3A%7B%22and%22%3A%5B%7B%22name%22%3A%7B%22like%22%3A%22%25Casa%25%22%7D%7D%2C%7B%22stars%22%3A4%7D%5D%7D%7D